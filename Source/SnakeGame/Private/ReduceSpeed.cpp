// Fill out your copyright notice in the Description page of Project Settings.


#include "ReduceSpeed.h"
#include "SnakeBase.h"

// Sets default values
AReduceSpeed::AReduceSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AReduceSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AReduceSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AReduceSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval((Snake->MovementSpeed)*1.3);
			Destroy();
		}
	}
}

