// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);//�������� ��������� �� ������
		if (IsValid(Snake))
		{
			Snake->AddSnakeElements();
			Destroy();
			TArray <AActor*> Out;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), FoodClass, Out);
			if (Out.Num() == 0)
			{
				addFoodElement();
			}

		}
	}
	else 
	{
		Destroy();
		addFoodElement();
	}
}

void AFood::addFoodElement()
{
	int X, Y;
	X = rand()%850-500;
	Y = rand()%670 - 670;
	FVector NewLocation(X, Y, 0);
	FTransform NewTransform = FTransform(NewLocation);
	AFood*NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	//NewFood->
}

